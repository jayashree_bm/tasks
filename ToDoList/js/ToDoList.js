var toDoList = {};
$(document).ready(function(){
	toDoList = {
		init: function(){
			this._bindEvents();
		},
		_bindEvents: function(){
			$('#addElementBtn').on('click', function(){
				toDoList.addNewElement();
			});
			$('ul').on('click', 'a.closeBtn', function() {
				this.parentElement.style.display = "none";
			});
			$('ul').on('click','input.editButton', function(){
				this.parentElement.setAttribute("contentEditable", true);
			});
			$('ul').on('click','a.doneBtn', function(){
				this.parentElement.style.background = "green";
			});
		},
		addNewElement: function(){
			var newLi = document.createElement("li");
			var input_Text = document.getElementById("userInput").value;
			var text_node = document.createTextNode(input_Text);
			newLi.appendChild(text_node);
			if (input_Text === '') {
				alert("enter value");
			} 
			else{
				document.getElementById("ulList").appendChild(newLi);
			}
			document.getElementById("userInput").value = "";

			var anchor = document.createElement("A");
			var symbol = document.createTextNode("\u00D7");
			anchor.className = "closeBtn";
			anchor.appendChild(symbol);
			newLi.appendChild(anchor);

			var aTag = document.createElement("A");
			var checkSymbol = document.createTextNode("\u2713");
			aTag.className = "doneBtn";
			aTag.appendChild(checkSymbol);
			newLi.appendChild(aTag);

			var edit_btn = document.createElement("INPUT");
			edit_btn.className = "editButton";
			edit_btn.setAttribute('value', "EDIT");
			edit_btn.setAttribute('type', "button");
			newLi.appendChild(edit_btn);
		}
	}
	toDoList.init();
});