var stopwatch = {};
		$(document).ready(function(){
			//alert("ready");
				var time = 0, miliseconds = 0, seconds = 0, minutes = 0, hours = 0;
				stopwatch = {
						init: function(){
							this.start();
							this.stop();
							this.clear();
						},

						countTime: function(){
							//alert("miliseconds");
							miliseconds++;
							//alert(miliseconds);
							if (miliseconds >= 60) {
								miliseconds = 0;
								seconds++;
								if (seconds >= 60) {
									seconds = 0;
									minutes++;
								}
								if (minutes >= 60) {
									minutes = 0;
									hours++;
								}
							}
							$(".displayTime").text(hours + ":" + minutes + ":" + seconds + ":" + miliseconds);
						},
						

						timer: function(){
							//alert("timer");
							time = setInterval(function(){
							   stopwatch.countTime();
							}, 10);
						},


						start: function(){
							$('#start').on('click', function(){
								stopwatch.timer();
								$('#start').attr("disabled", true);
							});
						},

						stop: function(){
							$('#stop').on('click', function(){
								clearTimeout(time);
								$('#start').attr("disabled", false);
							});
						},

						clear: function(){
							$('#clear').on('click', function(){
								clearTimeout(time);
								miliseconds = 0;
								seconds = 0;
								minutes = 0;
								hours = 0;
								time = 0;
								$('#start').attr("disabled", false);
								$(".displayTime").text(hours + ":" + minutes + ":" + seconds + ":" + miliseconds);
							});
						}

			}
			stopwatch.init();
			
		});